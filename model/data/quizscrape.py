#!/usr/bin/python 
import json
from time import sleep
import atexit

from requests import get
from bs4 import BeautifulSoup


def extract_quiz(markup):
    extracted_quiz = {}

    soup = BeautifulSoup(markup, "html5lib")

    for script in soup.find_all('script', type='text/x-config'):
        content = json.loads(script.text)
        if 'context' in content.keys():
            extracted_quiz['title'] = filter_excess_markup(content['context']['page']['title'])
        else:
            return

        if 'subbuzz' in content.keys():
            extracted_quiz['questions'] = []

            for question_index, question in enumerate(
                content['subbuzz']['questions']):
                if question['header']:
                    extracted_quiz['questions'].append(
                        {
                            'question': filter_excess_markup(question['header']),
                            'answers': []
                        })
                elif 'image_text' in question.keys():
                    extracted_quiz['questions'].append(
                        {
                            'question': filter_excess_markup(question['image_text']),
                            'answers': []
                        })
                else:
                    return

                for option in question['answers']:
                    if option['header']:
                        extracted_quiz[
                        'questions'][
                            question_index][
                                'answers'].append(
                                    {'option': filter_excess_markup(option['header'])})
                    elif 'image_text' in option.keys():
                        extracted_quiz[
                        'questions'][
                            question_index][
                                'answers'].append(
                                    {'option': filter_excess_markup(option['image_text'])})
                    else:
                        return

            extracted_quiz['results'] = []

            for result in content['subbuzz']['results']:
                extracted_quiz['results'].append({'result': filter_excess_markup(result['header'])})
        else:
            return

    return extracted_quiz


def filter_excess_markup(quiz_text):
    return BeautifulSoup(quiz_text, 'html5lib').text

    
def save_data(file_data):
    with open('./results/buzzfeed_quiz_data.json', 'w') as f:
        json.dump(file_data, f)
        print("Data written to file")

def load_data():
    with open('./results/buzzfeed_quiz_data.json', 'r') as f:
        return json.load(f)

def build_file_data(num_pages=1000):
    try:
        return load_data()
    except Exception:
        file_data = {'quizzes': []}

        for page_idx in range(num_pages):
            print(f"{page_idx}/{num_pages} pages of urls retrieved")

            res = get(
                f"https://www.buzzfeed.com/api/v2/feeds/quizzes/?p={page_idx}",
                headers=headers)

            for buzz in res.json()['buzzes']:
                file_data['quizzes'].append({'uri': buzz['canonical_path'], 'quiz': {}})

        return file_data

if __name__ == '__main__':
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) ' + \
        'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36'}

    file_data = build_file_data()

    atexit.register(save_data, file_data)

    for quiz_index, quiz in enumerate(file_data['quizzes']):
        print(f"{quiz_index}/{len(file_data['quizzes'])} quizzes retrieved")

        if not file_data['quizzes'][quiz_index]['quiz']:

            quiz_markup = get(f"https://www.buzzfeed.com{quiz['uri']}", headers=headers).text

            extracted = extract_quiz(quiz_markup)

            if extracted:
                file_data['quizzes'][quiz_index]['quiz'] = extracted
