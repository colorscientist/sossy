import json
from pprint import pprint

with open('./top10k.data.adj') as top10k_file:
    with open('../model/nn_modules/pretrained/vocabulary.json') as vocab_file:
        with open('./top10k_adj.json', 'w') as top10k_vocab_file:
            vocab_data = json.load(vocab_file)
            top10k_data = top10k_file.read().splitlines()
            shared_vocab = {}
            for vocab_pair in vocab_data.items():
                if vocab_pair[0] in top10k_data or vocab_pair[1] < 10:
                    shared_vocab[len(shared_vocab)] = vocab_pair[0]

            json.dump(shared_vocab, top10k_vocab_file)
