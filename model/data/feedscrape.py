"""
Acquires the buzzfeed quiz index, from the front page to page num_pages-1.
A word to the wise: Does not currently support partial downloads, in
any way, shape, or form. Cba ¯\_(ツ)_/¯
"""

import requests
import json
import time

with open('results/data.json', 'w') as f:
    articles = []
    num_pages = 1000
    for p in range(num_pages):
        res = requests.get(
            f"https://www.buzzfeed.com/api/v2/feeds/quizzes/?p={p}")
        if(res.json()['success'] == 1):
            print(num_pages-p)
            articles.append(res.json())
        else:
            print(f'Request failed for page {p}')
        time.sleep(0.15)

    json.dump(articles, f)
    print("Data download complete")
