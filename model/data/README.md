# A Note

Nothing in results should be counted on as continuing to be there. If it needs to continue to be there, the data/ directory should instead contain a script which retrieves or creates it.