import json
""" from PIL import Image, ImageDraw, ImageFont"""
from create_tfrecords import create
import glob


""" def title_to_image():
    with open('results/buzzfeed_feed_data.json') as f:
        loaded = json.load(f)

        for i in range(len(loaded)):
            for j in range(len(loaded[i]['buzzes'])):
                title = loaded[i]['buzzes'][j]['title']
                font = ImageFont.truetype(
                    '/usr/share/fonts/adobe-source-code-pro/' +
                    'SourceCodePro-Regular.otf')
                img = Image.new('RGB', (1000, 50), (255, 255, 255))
                d = ImageDraw.Draw(img)
                d.text((20, 20), title, fill=(0, 0, 0), font=font)
                img.save(
                    f'./results/title_images/imagep{i}t{j}.png',
                    format="PNG"
                    )
                img.close()
            print(f'Pages remaining: {len(loaded)-i}') """


def images_to_tfrecord():
    imagedirs = glob.glob("./results/title_images/*.png")
    images = []
    for i in range(len(imagedirs)):
        images.append({
            "filename": imagedirs[i],
            "id": i,
            "class": {"label": 1}
        })
        
    print create(
        dataset=images,
        dataset_name="buzzfeed_title_images",
        output_directory="./results/tfrecords/buzzfeed_title_images",
        num_shards=10,
        num_threads=5,
        store_images=True
    )


images_to_tfrecord()
