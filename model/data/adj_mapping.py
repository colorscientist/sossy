import json

with open('./top10k_adj.json') as adj_file:
    with open('../model/nn_modules/pretrained/vocabulary.json') as vocab_file:
        with open('./adj_mapping.json', 'w') as adj_mapping_file:
            adj_json = json.loads(adj_file.read())
            vocab_json = json.loads(vocab_file.read())
            mapping = {}
            for index, value in enumerate(adj_json.values()):
                mapping[index] = vocab_json[value]

            print(mapping)
            json.dump(mapping, adj_mapping_file)
