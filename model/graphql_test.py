import nose
from nose.tools import ok_, eq_, raises
import graphql as graphql


quiz = {
    'title': 'Title',
    'questions': [
        {
            'question': 'Question 1?',
            'options': [
                {
                    'option': 'Option 1',
                    'result_support_vector': [0.2, 0.4, 1.1]
                },
                {
                    'option': 'Option 2',
                    'result_support_vector': [0.0, -1.0, -0.5]
                }
            ]
        },
        {
            'question': 'Question 2?',
            'options': [
                {
                    'option': 'Option 3',
                    'result_support_vector': [0.0, 0.0, 0.0]
                },
                {
                    'option': 'Option 4',
                    'result_support_vector': [0.2, 0.2, 0.2]
                }
            ]
        }
    ],
    'results': [
        {
            'result': 'Result 1',
            'result_desc': 'Result 1 Description'
        },
        {
            'result': 'Result 2',
            'result_desc': 'Result 2 Description'
        }
    ]
}


def test_assemble_query():
    expected_query = {'query': """\
mutation {addQuiz(input: { title: "Title", \
questions: [{ question: "Question 1?", options: \
[{ option: "Option 1", resultSupportVector: [0.2, 0.4, 1.1]}, \
{ option: "Option 2", resultSupportVector: [0.0, -1.0, -0.5]}]}, \
{ question: "Question 2?", options: \
[{ option: "Option 3", resultSupportVector: [0.0, 0.0, 0.0]}, \
{ option: "Option 4", resultSupportVector: [0.2, 0.2, 0.2]}]}], \
results: [{ result: "Result 1", resultDesc: "Result 1 Description"}, \
{ result: "Result 2", resultDesc: "Result 2 Description"}] }){ id }}"""}

    eq_(graphql.build_mutation(quiz), expected_query)


def test_get_post_type_on_quiz():
    eq_(graphql.get_post_type(quiz), 'Quiz')

def test_get_post_type_on_article():
    article = {'title': "Title", 'body': ['Paragraph 1', 'Paragraph 2']}

    eq_(graphql.get_post_type(article), 'Article')

@raises(TypeError)
def test_build_query_no_post_type():
    graphql.get_post_type({ 'non_field': "Not a field" })

def test_build_options_string():
    options = [
        { 'option': "Option 1", 'result_support_vector': [0.1, 0.2]}, 
        { 'option': "Option 2", 'result_support_vector': [0.2, -0.4]}
        ]

    expected_options_string = """
    [{ option: "Option 1", resultSupportVector: [0.1, 0.2]}, { option: "Option 2", resultSupportVector: [0.2, -0.4]}]
    """.strip()

    eq_(graphql.build_options_string(options), expected_options_string)

def test_build_results_string():
    results = [
        { 'result': 'Result', 'result_desc': 'Result Description'}
    ]

    expected_results_string = """
    [{ result: "Result", resultDesc: "Result Description"}]
    """.strip()

    eq_(graphql.build_results_string(results), expected_results_string)

def test_build_questions_string():
    questions = [
        {
            'question': 'Question',
            'options': [
                {
                    'option': 'Option',
                    'result_support_vector': [0.1]
                }
            ]
        }
    ]

    expected_questions_string = """
    [{ question: "Question", options: [{ option: "Option", resultSupportVector: [0.1]}]}]
    """.strip()

    eq_(graphql.build_questions_string(questions), expected_questions_string)