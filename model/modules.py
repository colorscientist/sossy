"""
`They live their days in mass delusion
while I coddle my seclusion`

Torch nn.Modules, for use as components in nets
"""

import torch
import torch.nn as nn
import torch.nn.functional as F


class Visual(nn.Module):
    """
    Takes in a picture and returns a map of features within it, each one centered around
    their location
    """
    def __init__(self, num_blocks=3):
        super().__init__()

        self.initial_conv = nn.Conv2d(3, 16, 7)

        self.visual_blocks = [
            VisualBlock() \
            for i \
            in range(num_blocks)
        ]

    # pylint: disable=W0221
    def forward(self, x):
        initial_conv_and_activation = F.relu(self.initial_conv(x))

        visual_output = initial_conv_and_activation

        return visual_output


class VisualBlock(nn.Module):
    """
    At a single visual resolution, convolve over an area with multiple
    layers connected via residual connections. Defined by layers with
    the same receptive field.
    """
    def __init__(self, block_size=3):
        super().__init__()

        self.block_size = block_size

        self.conv_layers = [
            nn.Conv2d(in_channels=3, out_channels=3, kernel_size=3) \
            for i \
            in range(block_size)
        ]

        self.bn_layers = [
            nn.BatchNorm2d(num_features=3) \
            for i \
            in range(block_size)
        ]

    # Disable warning that our parameters differ from the overridden
    # method - They're supposed to.
    # pylint: disable=W0221
    def forward(self, x):

        normalized = self.bn_layers[0](x)

        for i in range(self.block_size - 1):
            convolved = self.conv_layers[i](normalized) + normalized
            normalized = self.conv_layers[i + 1](convolved)

        final_conv = self.conv_layers[-1](normalized)

        x = x + final_conv

        return x

    @staticmethod
    def _get_padding(padding_type, kernel_size):
        """
        Returns the padding for a particular convolution kernel size and
        padding strategy. Takes in padding_type and kernel_size. \n
        `padding_type` must be one of 'SAME' or 'VALID' \n
        `kernel_size` is a single number representing both width and
        height of the kernel.
        """

        assert padding_type in ['SAME', 'VALID'], \
            "padding_type must be one of 'SAME' or 'VALID'"

        if padding_type == 'SAME':
            return tuple((k - 1) // 2 for k in kernel_size)

        return tuple(0 for _ in kernel_size)

class AdjChooser(nn.Module):
    """
    A PyTorch module designed to pick sets of adjectives which can be
    used as the basis for our quizzes.
    """
    def __init__(self, adj_list_len=4):
        super().__init__()
        with open('../../data/top10k_adj.json') as top10k_file:
            print("Reading adjectives file")
            top10k = json.load(top10k_file)
            self.top10k = {int(k):v for k, v in top10k.items()}

        with open('./pretrained/glove.840B.300d.txt') as vectors_file:
            print("Reading vectors file")
            next_line = vectors_file.readline()
            embeds_list = []
            while next_line:
                vector = next_line.rstrip('\n').split(' ')
                if vector[0] in top10k.values():
                    embeds_list.append([float(x) for x in vector[1:]])
                next_line = vectors_file.readline()

        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

        self.vocab_size = len(embeds_list)

        self.embed_dims = len(embeds_list[0])

        print("Loading embeddings")

        self.embeds = nn.Embedding(
            num_embeddings=self.vocab_size,
            embedding_dim=self.embed_dims
            )

        embeds_dict = self.embeds.state_dict()

        embeds_dict.update({'weight': torch.tensor(embeds_list)})

        self.embeds.load_state_dict(embeds_dict)

        self.hidden_size = 512

        print("Model ready")

    # pylint: disable=W0221
    def forward(self):
        embeddings = self.embeds(randoms)

class PredQuestionTopic(nn.Module):
    def __init__(self):
        pass

    def forward(self, x):
        pass