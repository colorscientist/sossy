"""
Our hearts just won't die \n
It's the trip that keeps us alive
"""

import torch
from torchvision import transforms


def current_frame_to_torch(current_frame):
    """
    Takes the current frame as a PIL Image, and returns a Tensor which
    contains it in NWHC form
    """
    normalize = transforms.Normalize(
        mean=[0.485, 0.456, 0.406],
        std=[0.229, 0.224, 0.225])

    pil_to_torch = transforms.ToTensor()

    return normalize(pil_to_torch(current_frame)).unsqueeze(0)
