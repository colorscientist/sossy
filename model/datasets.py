"""
She
She's figured out
That all her doubts were someone else's point of view
"""

import json

import torch
from torch.utils.data import Dataset, DataLoader
import spacy

class OptionsDataset(Dataset):
    def __init__(
        self,
        data_path='../../data/', 
        data_file='results/buzzfeed_quiz_data.json', 
        vocab_file='vocabulary.json'
        ):

        super().__init__()

        self.data = []

        with open(data_path+data_file) as f:
            self.quiz_json = json.load(f)

    def __len__(self):
        count = 0
        for quiz in self.quiz_json['quizzes']:
            print(quiz)
            for question in quiz['quiz']['questions']:
                count += len(question['answers'])

    def __getitem__(self, idx):
        pass


class QuestionsDataset(Dataset):
    def __init__(
        self, 
        data_path='../../data/', 
        data_file='results/buzzfeed_quiz_data.json', 
        vocab_file='vocabulary.json',
        spacy_model='en_core_web_sm'
        ):

        super().__init__()

        self.data = []

        with open(data_path+data_file) as f:
            self.quiz_json = json.load(f)

        self.nlp = spacy.load(spacy_model)

    def __len__(self):
        count = 0
        for quiz in self.quiz_json['quizzes']:
            for question in quiz['quiz']['questions']:
                count += len(question['answers'])

    def __getitem__(self, idx):
        pass