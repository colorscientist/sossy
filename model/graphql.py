"""
`If I told you that a flower bloomed in a dark room, \
would you trust it?`

Various functionality related to the graphql api, including \
connectivity checks and functions for uploading data created \
by the model
"""
import json

import requests

from constants import GRAPHQL_URL


def is_connected():
    """
    Check our connection to the graphql server \n
    Returns a boolean representing whether we're connected
    """

    query = {"query": "{ __schema { types { name }}}"}

    resp = requests.post(GRAPHQL_URL, json={'query': query}, headers=headers)

    return '__schema' in resp['data'].keys()

def get_post_type(post):
    post_keys = set(post.keys())
    quiz_keys = set(['title', 'questions', 'results'])
    article_keys = set(['title', 'body'])

    if quiz_keys.issubset(post_keys):
        return 'Quiz'
    elif article_keys.issubset(post_keys):
        return 'Article'
    else:
        raise TypeError("Post is not of any valid type")

def build_results_string(results):
    result_string_array = [
        '{{ result: "{0}", resultDesc: "{1}"}}'.format(
            result['result'], result['result_desc']) \
        for result \
        in results
    ]

    results_string = """
    [{0}]
    """.format(', '.join(result_string_array)).strip()

    return results_string

def build_options_string(options):
    options_string_array = [
        '{{ option: "{0}", resultSupportVector: {1}}}'.format(
            option['option'], option['result_support_vector']) \
        for option \
        in options
        ]

    options_string = """
    [{0}]
    """.format(', '.join(options_string_array)).strip()

    return options_string

def build_questions_string(questions):
    questions_string_array = []

    for question in questions:
        question_string = '{{ question: "{0}", options: {1}}}'.format(
            question['question'], build_options_string(question['options'])
        )
        questions_string_array.append(question_string)
    
    questions_string = """
    [{0}]
    """.format(', '.join(questions_string_array)).strip()

    return questions_string

def build_mutation(post):
    post_type = get_post_type(post)

    if post_type is 'Quiz':
        questions = build_questions_string(post['questions'])
        results = build_results_string(post['results'])
        mutation_string = """mutation \
{{addQuiz(input: {{ title: "{0}", questions: {1}, results: {2} }})\
{{ id }}}}""".format(post['title'], questions, results).strip()
    elif post_type is 'Article':
        mutation_string = '{{ }}'

    query = { 'query': mutation_string }

    return query

def upload(post):
    query = build_mutation(post)

    if query is not None:

        resp = requests.post(GRAPHQL_URL, json=query)

    return resp


if __name__ == '__main__':
    quiz = {
        'title': 'Title',
        'questions': [
            {
                'question': 'Question 1?',
                'options': [
                    {
                        'option': 'Option 1',
                        'result_support_vector': [0.2, 0.4, 1.1]
                    },
                    {
                        'option': 'Option 2',
                        'result_support_vector': [0.0, -1.0, -0.5]
                    }
                ]
            },
            {
                'question': 'Question 2?',
                'options': [
                    {
                        'option': 'Option 3',
                        'result_support_vector': [0.0, 0.0, 0.0]
                    },
                    {
                        'option': 'Option 4',
                        'result_support_vector': [0.2, 0.2, 0.2]
                    }
                ]
            }
        ],
        'results': [
            {
                'result': 'Result 1',
                'result_desc': 'Result 1 Description'
            },
            {
                'result': 'Result 2',
                'result_desc': 'Result 2 Description'
            }
        ]
    }

    resp = upload(quiz)

    print(resp.text)