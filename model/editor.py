from PIL import ImageDraw, ImageFont, Image
import cProfile
import pstats
from itertools import count
from timer import RepeatedTimer
import numpy as np


class Editor:
    def __init__(
            self,
            data={
                'title': ' ',
                'questions': [{
                    'question': ' ',
                    'options': [{
                        'option': ' '
                    }]
                }],
                'results': [{
                    'result': ' '
                }],
                'postType': ' '}):
        """
        """

        self.max_line_len = 120

        self.frame_width = 1000

        self.crop_box = (0, 0, 1000, 400)

        self.xpad = 10

        self.ypad = 10

        self.data = data

        self.replace_lists(data)

        self.cursor_pos = (0, 0)

        # get a font
        self.title_font = ImageFont.truetype(
            '/usr/share/fonts/' +
            'adobe-source-code-pro/SourceCodePro-Regular.otf', 32)

        self.title_size = self.title_font.getsize(self.data['title'])

        self.default_font = ImageFont.truetype(
            '/usr/share/fonts/' +
            'adobe-source-code-pro/SourceCodePro-Regular.otf', 12)

        self.default_size = self.default_font.getsize(
            self.data['questions'][0]['question'])

        self.current_frame = Image.new(
            'RGBA',
            (self.frame_width, 0),
            (255, 255, 255, 255))

        # get a drawing context
        self.draw_context = ImageDraw.Draw(self.current_frame)

        self.title_frame = Image.new(
            'RGBA',
            (self.frame_width, self.title_size[1] + self.ypad))

        self.title_draw_context = ImageDraw.Draw(self.title_frame)

        self.question_frames = [
            Image.new(
                'RGBA',
                (self.frame_width, self.default_size[1] + self.ypad)
                )
            for i
            in range(len(self.data['questions']))
        ]

        self.question_contexts = [
            ImageDraw.Draw(self.question_frames[i])
            for i
            in range(len(self.question_frames))
        ]

        self.option_frames = [
            [
                Image.new(
                    'RGBA',
                    (self.frame_width, self.default_size[1] + self.ypad))
                for j
                in range(len(self.data['questions'][i]['options']))
            ]
            for i
            in range(len(self.question_frames))
        ]

        self.option_contexts = [
            [
                ImageDraw.Draw(self.option_frames[i][j])
                for j
                in range(len(self.option_frames[i]))
            ]
            for i
            in range(len(self.option_frames))
        ]

        self.result_frames = [
            Image.new(
                'RGBA',
                (self.frame_width, self.default_size[1] + self.ypad))
            for i
            in range(len(self.data['results']))
        ]

        self.result_contexts = [
            ImageDraw.Draw(self.result_frames[i])
            for i
            in range(len(self.result_frames))
        ]

        self.cursor_fill = (0, 0, 0, 255)

        def blink_cursor():
            if self.cursor_fill[3] == 255:
                self.cursor_fill = (0, 0, 0, 0)
            else:
                self.cursor_fill = (0, 0, 0, 255)

        RepeatedTimer(.5, blink_cursor)

        self.render_all()

    def render_all(self):
        """
        Render our current quiz into the drawing context. \n
        """

        self.render_title(self.data['title'])

        for question_index, question_value \
        in self.data['questions'].items():
            self.render_question(
                question_value, index=question_index)

            for option_index, option_value \
            in question_value['options'].items():

                self.render_option(
                    option_value,
                    index=option_index,
                    question_index=question_index
                    )

        for result_index, result_value in self.data['results'].items():
            self.render_result(
                result_value, index=result_index)

        self.render_cursor(self.data)

        total_size = self.calculate_size()

        if total_size <= 400:
            total_size = 400

        self.current_frame = Image.new(
            'RGBA',
            (self.frame_width, total_size),
            (255, 255, 255, 255))

        self.composite_rendered_results()

        self.crop()

        self.convert_image_type()

    def convert_image_type(self):
        tmp = Image.new("RGB", self.current_frame.size, (255, 255, 255))
        tmp.paste(self.current_frame)

        self.current_frame = tmp

    def crop(self):
        self.current_frame.crop(self.crop_box)

    def calculate_size(self):
        total_size = self.title_frame.size[1]

        total_size += sum(
            [self.question_frames[i].size[1]
             for i
             in range(len(self.question_frames))])

        total_size += sum(
            [self.result_frames[i].size[1]
             for i
             in range(len(self.result_frames))])

        for option_frame_group in self.option_frames:
            for option_frame in option_frame_group:
                total_size += option_frame.size[1]

        total_size += self.ypad

        return total_size

    def composite_rendered_results(self):
        self.current_frame.alpha_composite(self.title_frame)

        marker = self.title_frame.size[1]

        for question_index, question_frame \
        in enumerate(self.question_frames):

            self.current_frame.alpha_composite(
                question_frame, (self.xpad*2, marker))
            marker += question_frame.size[1]

            for option_index, option_frame \
            in enumerate(self.option_frames[question_index]):
                self.current_frame.alpha_composite(
                    option_frame, (self.xpad * 3, marker)
                )
                marker += option_frame.size[1]

        for result_index, result_frame \
        in enumerate(self.result_frames):
            self.current_frame.alpha_composite(
                result_frame, (self.xpad*2, marker))
            marker += result_frame.size[1]

    def find_cursor_line(self, data, counter=0, loc_list=[]):
        if isinstance(data, list):
            data = dict(zip(count(0), data))
        for index, value in data.items():
            if isinstance(value, str) and counter == self.cursor_pos[0]:
                loc_list.append(index)
                return value, loc_list
            elif isinstance(value, str):
                counter += 1
            elif isinstance(value, dict):
                loc_list.append(index)
                recurse = self.find_cursor_line(
                    value, counter, loc_list=loc_list)
                if recurse[0] is not None:
                    return recurse
                else:
                    counter = recurse[1]
        else:
            return None, counter

    def nested_set(self, dic, keys, value):
        for key in keys[:-1]:
            dic = dic.setdefault(key, {})
        dic[keys[-1]] = value

    def delete_char(self):
        line_data, line_loc = self.find_cursor_line(self.data, loc_list=[])
        line_data = [*line_data]
        del line_data[self.cursor_pos[1]]
        line_data = ''.join(line_data)
        self.nested_set(self.data, line_loc, line_data)

    def add_char(self, char):
        line_data, line_loc = self.find_cursor_line(self.data, loc_list=[])
        line_data = [*line_data]
        line_data.insert(self.cursor_pos[1], char)
        line_data = ''.join(line_data)
        self.nested_set(self.data, line_loc, line_data)

    def render_title(self, value):
        self.title_draw_context.text(
            (self.xpad, self.ypad),
            f"Title: {value}",
            font=self.title_font,
            fill=(0, 0, 0, 255)
        )

    def render_question(self, value, index):
        self.question_contexts[index].text(
                (self.xpad*2, self.ypad),
                f"Question {index}: {value['question']}",
                font=self.default_font,
                fill=(0, 0, 0, 255)
            )

    def render_option(self, value, index, question_index):
        self.option_contexts[question_index][index].text(
                (self.xpad*3, self.ypad),
                f"Option {index}: {value['option']}",
                font=self.default_font,
                fill=(0, 0, 0, 255)
            )

    def render_result(self, value, index):
        self.result_contexts[index].text(
            (self.xpad*2, self.ypad),
            f"Result {index}: {value['result']}",
            font=self.default_font,
            fill=(0, 0, 0, 255)
        )

    def render_cursor(self, data):
        line_data, line_loc = self.find_cursor_line(self.data, loc_list=[])

        if line_loc[-1] == 'title':
            xlen = self.title_font.getsize(
                "Title: " + line_data[:self.cursor_pos[1]])

            self.title_draw_context.line(
                [
                    (self.xpad + xlen[0] - 2, self.ypad * 2),
                    (self.xpad + xlen[0] - 2,
                     self.ypad * 4 + self.default_size[1] - 9)
                ],
                fill=self.cursor_fill,
                width=1)

        if line_loc[-1] == 'question':
            xlen = self.default_font.getsize(
                "Question 0: " + line_data[:self.cursor_pos[1]])

            self.question_contexts[line_loc[-2]].line(
                [
                    (self.xpad*2 + xlen[0] - 2, self.ypad + 3),
                    (self.xpad*2 + xlen[0] - 2,
                     self.ypad + self.default_size[1] - 3)
                ],
                fill=self.cursor_fill,
                width=1)

        if line_loc[-1] == 'option':
            xlen = self.default_font.getsize(
                "Option 0: " + line_data[:self.cursor_pos[1]])

            self.option_contexts[line_loc[-2]].line(
                [
                    (self.xpad*3 + xlen[0] - 2, self.ypad + 3),
                    (self.xpad*3 + xlen[0] - 2,
                     self.ypad + self.default_size[1] - 3)
                ],
                fill=self.cursor_fill,
                width=1)

        if line_loc[-1] == 'result':
            xlen = self.default_font.getsize(
                "Result 0: " + line_data[:self.cursor_pos[1]])

            self.result_contexts[line_loc[-2]].line(
                [
                    (self.xpad*2 + xlen[0] - 2, self.ypad + 3),
                    (self.xpad*2 + xlen[0] - 2,
                     self.ypad + self.default_size[1] - 3)
                ],
                fill=self.cursor_fill,
                width=1)

    def replace_lists(self, data, loc_list=[]):
        if isinstance(data, dict):
            for index, value in data.items():
                loc_list.append(index)
                self.replace_lists(value, loc_list=loc_list)
                loc_list.pop()

        if isinstance(data, list):
            dictified = dict(zip(count(0), data))
            self.nested_set(self.data, loc_list, dictified)
            for index, value in dictified.items():
                loc_list.append(index)
                self.replace_lists(value, loc_list=loc_list)
                loc_list.pop()

    def move_cursor(self, direction):
        if direction == 'up' and self.cursor_pos[0] != 0:
            self.cursor_pos = (self.cursor_pos[0] - 1, self.cursor_pos[1])
            return True

        if direction == 'down' and self.cursor_pos[0] != 0:
            self.cursor_pos = (self.cursor_pos[0] + 1, self.cursor_pos[1])
            return True

        if direction == 'left' and self.cursor_pos[1] != 0:
            self.cursor_pos = (self.cursor_pos[0], self.cursor_pos[1] - 1)
            return True

        line_len = len(self.find_cursor_line(self.data, loc_list=[])) - 1
        if direction == 'right' and self.cursor_pos[1] != line_len:
            self.cursor_pos = (self.cursor_pos[0], self.cursor_pos[1] + 1)
            return True

        return False

    def editor_iterator(self):
        while True:
            self.render_all()

            box = (0, 0, 1000, 250)

            yield np.array(self.current_frame.crop(box), dtype=np.float32)


if __name__ == '__main__':
    """     editor = Editor(data={
        'title': 'You will believe this rendering method',
        'questions': [
            {'question': 'Why?',
             'options': [{'option': 'Opt 1'}, {'option': 'Opt 2'}]
             },
            {'question': 'What?',
             'options': [{'option': 'Opt 3'}, {'option': 'Opt 4'}]
             }
             ],
        'results': [{'result': 'This is the result of your folly'}]
        }) """

    editor = Editor()

    pr = cProfile.Profile()
    pr.enable()

    editor.cursor_pos = (0, 0)

    editor.move_cursor('right')

    editor.render_all()

    print(np.array(editor.current_frame).shape)

    pr.disable()
    ps = pstats.Stats(pr).sort_stats('cumulative')

    editor.current_frame.save('./tmp.png')
