"""
I was trying to help, but I guess I pushed too hard \n
Now we can't even touch it, afraid it'll fall apart
"""

import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision import transforms

from editor import Editor
from inputs import current_frame_to_torch
from nn_modules.visual import Visual

class Net(nn.Module):
    """
    Represents the entire model, from start to finish
    """
    def __init__(self, dtype=torch.float):
        super().__init__()

        self.device = torch.device(
            'cuda' if torch.cuda.is_available() else 'cpu'
            )

        assert dtype in [torch.float, torch.half, torch.double], \
            "Net dtype argument must be valid torch data type"

        self.dtype = dtype

        self.visual = Visual()

    # pylint: disable=W0221
    def forward(self, frame=None, cursor_feedback=None):
        if frame is not None:
            visual_output = self.visual(frame)

        if cursor_feedback is not None:
            tactile_output = self.tactile(cursor_feedback)

        if visual_output is not None:
            print("Success")


if __name__ == '__main__':
    editor = Editor()

    net = Net()

    cf = current_frame_to_torch(editor.current_frame)

    net(frame=cf)
