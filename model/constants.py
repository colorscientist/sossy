import os
from pathlib import Path

PROD = "PROD" in os.environ

PROJECT_ROOT = Path('.')

DATA_DIR = PROJECT_ROOT / 'data'

VOCAB = DATA_DIR / 'results' / 'vocabulary.json'

# Add production URL when available
GRAPHQL_URL = '' if PROD else 'http://127.0.0.1:4050/graphql'
