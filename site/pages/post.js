'use strict';

import App from '../components/App.js';
import fetch from 'isomorphic-unfetch';
import RandomButton from '../components/RandomButton';
import withData from '../lib/withData';
import Header from '../components/Header';
import Post from '../components/Post';
import { withRouter } from 'next/router';

export default withRouter(withData((props) => (
  <App>
  <Header pathname={props.router.pathname} />
  <Post title={props.router.query.url}/>
  <RandomButton />
  </App>
  )
))
