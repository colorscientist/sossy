'use strict';

import App from '../components/App'
import Header from '../components/Header'
import PostList from '../components/PostList'
import withData from '../lib/withData'
import RandomButton from '../components/RandomButton'
import { withRouter } from 'next/router';

export default withRouter(withData((props) => (
  <App>
    <Header pathname={props.router.pathname} />
    <h2>Quizzes</h2>
    <PostList postType="Quiz"/>
    <h2>Articles</h2>
    <PostList postType="Article"/>
    <RandomButton />
  </App>
)))