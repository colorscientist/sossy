'use strict';

export default ({message}) => (
    <aside>
      {message}
      <style jsx>{`
        aside {
          padding: 1.5em;
          font-size: 16px;
          color: white;
          background-color: red;
        }
      `}</style>
    </aside>
  )