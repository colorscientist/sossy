'use strict';

import React from 'react';
import Quiz from './Quiz';
import Article from './Article';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import ErrorMessage from './ErrorMessage';

class Post extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        if (this.props.data.error) return <ErrorMessage message={this.props.data.error.toString()} />
        if (this.props.data.post) {
        const post = this.props.data.post.postType === 'Quiz' ? <Quiz quiz={this.props.data.post}/> : 
                    this.props.data.post.postType === 'Article' ? <Article article={this.props.data.post}/> :
                    null
        return (
            <div>
                {post}
            </div>
            )
        }
    return <div>Loading</div>
    }
}

const post = gql`
  query post($title: String) {
    post(input: {url: $title}) {
      id
      title
      createdAt
      postType

      ... on Quiz {
          questions {
              question
              id
              options {
                  id
                  option
                  optionPic {
                      source
                      altText
                      imgDesc
                  }
                  resultSupportVector
              }
          }
          results {
              result
              resultDesc
              resultPic {
                  source
                  altText
                  imgDesc
              }
          }
      }

      ... on Article {
          body
      }
    }
  }
`

export default graphql(post, {
  options: (props) => ({ variables: { title: props.title }}),
})(Post)