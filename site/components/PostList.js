'use strict';

import { graphql } from 'react-apollo'
import gql from 'graphql-tag'
import ErrorMessage from './ErrorMessage'
import PostLink from './PostLink'
import { List } from 'semantic-ui-react'
import 'semantic-ui-css/components/list.css';
import { Button } from 'semantic-ui-react';
import 'semantic-ui-css/components/button.css';

const POSTS_PER_PAGE = 5

function PostList ({ data: { loading, error, posts, _postsMeta }, loadMorePosts }) {
  if (error) return <ErrorMessage message={error.toString()} />
  if (posts && posts.length) {
    const areMorePosts = posts.length < _postsMeta.count
    return (
      <section>
        <List link relaxed>
          {posts.map((post, index) =>
              <List.Item key={index}>
                <PostLink post={post}/>
              </List.Item>
          )}
        </List>
        {areMorePosts ? <Button onClick={() => loadMorePosts()}> {loading ? 'Loading...' : 'Show More'} </Button> : ''}
        <style jsx>{`
          section {
            padding-bottom: 20px;
          }
          li {
            display: block;
            margin-bottom: 10px;
          }
          div {
            align-items: center;
            display: flex;
          }
          a {
            font-size: 16px;
            margin-right: 10px;
            text-decoration: none;
            padding-bottom: 0;
            border: 0;
          }
          span {
            font-size: 16px;
            margin-right: 5px;
          }
          ul {
            margin: 0;
            padding: 0;
          }
        `}</style>
      </section>
    )
  }
  return <div>Loading</div>
}

const posts = gql`
  query posts($first: Int!, $skip: Int!, $postType: String!) {
    posts(input: { first: $first, skip: $skip, postType: $postType }) {
      id
      title
      url
      createdAt
    }
    _postsMeta(input: { postType: $postType }) {
      count
    }
  }
`

// The `graphql` wrapper executes a GraphQL query and makes the results
// available on the `data` prop of the wrapped component (PostList)
export default graphql(posts, {
  options: (props) => ({
    variables: {
      skip: 0,
      first: POSTS_PER_PAGE,
      postType: props.postType
    }
  }),
  props: ({ data }) => ({
    data,
    loadMoreArticles: () => {
      return data.fetchMore({
        variables: {
          skip: data.posts.length
        },
        updateQuery: (previousResult, { fetchMoreResult }) => {
          if (!fetchMoreResult) {
            return previousResult
          }
          return Object.assign({}, previousResult, {
            // Append the new Articles results to the old one
            posts: [...previousResult.posts, ...fetchMoreResult.posts]
          })
        }
      })
    }
  })
})(PostList)
