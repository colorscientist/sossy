'use strict';

import React from 'react';
import Head from 'next/head'

export default ({ children, title='Sossy' }) => (
    <main>
      <Head>
      <title>{ title }</title>
      <meta charSet='utf-8' />
      <meta name='viewport' content='initial-scale=1.0, width=device-width' />
      <link rel="manifest" href="/static/manifest.json" />
      <link rel='stylesheet' href='/_next/static/style.css'/>
      </Head>
      {children}
      <style jsx global>{`
        * {
          font-family: Menlo, Monaco, "Lucida Console", "Liberation Mono", "DejaVu Sans Mono", "Bitstream Vera Sans Mono", "Courier New", monospace, serif;
        }
        body {
          margin: 0;
          padding: 25px 50px;
        }
        a {
          color: #22BAD9;
        }
        p {
          font-size: 16px;
          line-height: 24px;
        }
        article {
          margin: 0 auto;
          max-width: 650px;
        }
      `}</style>
    </main>
  )