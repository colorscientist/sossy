// src/ErrorMessage.test.js
import test from 'ava';
import React from 'react';
import ReactDOM from 'react-dom';
import ErrorMessage from './ErrorMessage';

test('renders without crashing', t => {
  const div = document.createElement('div');
  ReactDOM.render(<ErrorMessage message={"Oh no :("} />, div);
  t.pass();
});