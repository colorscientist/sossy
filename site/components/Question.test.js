import test from 'ava';
import React from 'react';
import ReactDOM from 'react-dom';
import Question from './Question';

test("It renders", t => {
    const div = document.createElement('div');
    ReactDOM.render(<Question />, div);
    t.pass();
})