'use strict';

export default function Article (props) {
  return (
    <section>
      <h1>{props.article.title}</h1>
      <p>{props.article.body.join('\n').replace(/<[/]?p>/g, '')}</p>
      <style jsx>{`
      section {
        padding-bottom: 20px;
      }
      li {
        display: block;
        margin-bottom: 10px;
      }
      div {
        align-items: center;
        display: flex;
      }
      a {
        font-size: 16px;
        margin-right: 10px;
        text-decoration: none;
        padding-bottom: 0;
        border: 0;
      }
      span {
        font-size: 16px;
        margin-right: 5px;
      }
      ul {
        margin: 0;
        padding: 0;
      }
      button:before {
        align-self: center;
        border-style: solid;
        border-width: 6px 4px 0 4px;
        border-color: #ffffff transparent transparent transparent;
        content: "";
        height: 0;
        margin-right: 5px;
        width: 0;
      }
    `}</style>
  </section>
  )
}

