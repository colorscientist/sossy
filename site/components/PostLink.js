'use strict';

import Link from 'next/link'

const PostLink = ({ post }) => (
  <div>
      <Link prefetch passHref as={`/p/${post.url}`} href={`/post?url=${post.url}&random=false`}>
        <a>{post.title}</a>
      </Link>
      <style jsx>{`
        li {
          list-style: none;
          margin: 5px 0;
        }
  
        a {
          text-decoration: none;
          color: blue;
          font-family: "Arial";
        }
  
        a:hover {
          opacity: 0.6;
        }
      `}</style>
    </div>
  )

export default PostLink;