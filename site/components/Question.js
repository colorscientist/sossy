'use strict';

import Option from './Option';
import { List } from 'semantic-ui-react'
import 'semantic-ui-css/components/list.css';

export default class Question extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div>
            <h3>{this.props.question.question}</h3>
            <List>
            {this.props.question.options.map((option, index) => 
            <List.Item key={index}>
            <Option name={option.option}
            picture={option.picture}
            handleOptionClick={this.props.handleOptionClick}
            questionindex={this.props.index}
            index={index}
            key={option.id}
            selectedOptions={this.props.selectedOptions}
            />
            </List.Item>
            )}
            </List>
            </div>
        )
    }    
}

