'use strict';

import Link from 'next/link'
import 'semantic-ui-css/components/menu.css';
import { Menu } from 'semantic-ui-react';

export default ({ pathname }) => (
  <Menu>
    <Menu.Item header>Sossy</Menu.Item>
    <Menu.Item>
      <Link prefetch href='/'>
        <a>Home</a>
      </Link>
    </Menu.Item>

    <Menu.Item>
      <Link prefetch href='/about'>
        <a>About</a>
      </Link>
    </Menu.Item>
  </Menu>
)
