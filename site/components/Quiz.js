'use strict';

import React from 'react';
import Question from './Question';
import Result from './Result';
import { List } from 'semantic-ui-react'
import 'semantic-ui-css/components/list.css';

export default class Quiz extends React.Component {
    constructor(props) {
        super(props)

        this.handleOptionClick = this.handleOptionClick.bind(this);

        this.state = {
            selectedOptions: Array.from(this.props.quiz.questions, (x) => null),
            givenResult: null,
        }
    }

    handleOptionClick(event, questionIndex, index) {
        this.setState((prevState) => {
            let tempArr = prevState.selectedOptions;
            tempArr[questionIndex] = index;
            return { selectedOptions: tempArr }
        }, () => {
            if (!this.state.selectedOptions.includes(null)) {
                this.calculateResult()
            }
        })
    }

    calculateResult() {
        let optionsArray = this.state.selectedOptions.map(
            (opt, ind) => { 
                return this.props.quiz.questions[ind].options[opt].resultSupportVector 
            });

        let summedOptions = optionsArray.reduce((acc, cur) => {
            return acc.map((num, ind) => {
                return num + cur[ind];
            });
          });
        this.setState((prevState) => {
            let result = this.indexOfMax(summedOptions)
            return { givenResult: result}
        })
    }

    indexOfMax(arr) {
        if (arr.length === 0) {
            return -1;
        }
    
        var max = arr[0];
        var maxIndex = 0;
    
        for (var i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                maxIndex = i;
                max = arr[i];
            }
        }
    
        return maxIndex;
    }

    render() {
        // Result does not render if options aren't selected
        const result = (this.state.givenResult != null) ? (
            <Result result={this.props.quiz.results[this.state.givenResult]} />
        ) : (
            null
        );

        return (
            <section>
            <h1>{this.props.quiz.title}</h1>
            <List>
            { this.props.quiz.questions.map((question, index) => 
            <List.Item key={index}>
                <Question question={question}
                index={index}
                handleOptionClick={this.handleOptionClick}
                key={question.id}
                selectedOptions={this.state.selectedOptions}/>
            </List.Item>
            )
            }
            </List>
            {result}
            </section>
        )
    }
}