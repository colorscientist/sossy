'use strict';

export default ({result}) => (
    <div>
    <h3>{result.result}</h3>
    <img src={result.resultPic.source} />
    <p>{result.resultDesc}</p>
    </div>
)