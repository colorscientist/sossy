'use strict';

import 'semantic-ui-css/components/button.css';
import { Button } from 'semantic-ui-react';
export default class Option extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
        <div>
        <Button
        onClick={(e) =>
        this.props.handleOptionClick(
            e, this.props.questionindex, this.props.index)}
        active={
            (this.props.selectedOptions[this.props.questionindex] === this.props.index)?
            true : 
            false
        }>
        {this.props.name}</Button>
        <style>`

        `</style>
        </div>
        )
    }
}
