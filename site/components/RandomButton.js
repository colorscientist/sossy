'use strict';

import Link from 'next/link';
import React from 'react';
import { Button } from 'semantic-ui-react';
import 'semantic-ui-css/components/button.css';

const RandomButton = ({props}) => (
    <Link as={`/p/random`} href={`/post?random=true`}>
        <Button> Random! </Button>
    </Link>
)

export default RandomButton;