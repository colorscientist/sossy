// Our mother has been absent ever since we founded Rome
// But there's going to be a party when the wolf comes home
'use strict';

const express = require('express');
const next = require('next');
const moduleAlias = require('module-alias');
const routes = require('./routes');
const compression = require('compression');
const cors = require('cors');
const { join } = require('path');
const { parse } = require('url');

const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handler = routes.getRequestHandler(app, ({req, res, route, query}) => {
  const parsedUrl = parse(req.url, true);
  const pathname = parsedUrl.pathname;
  console.log(__dirname)

  if (pathname === '/service-worker.js') {
    console.log("hello");
    const filePath = join(__dirname, '/.next', pathname);
    app.serveStatic(req, res, filePath);
  } else {
    console.log("goodbye");
    app.render(req, res, parsedUrl);
  }
});

app.prepare()
.then(() => {
  const server = express();

  server.use(cors({
    origin: ['http://localhost:'+port.toString()],
    credentials: true
  }));
  server.use(compression());
  server.use(handler);

  server.listen(port, (err) => {
    if (err) throw err
    console.log('> Ready on http://localhost:' + port)
  })
})
.catch((ex) => {
  console.error(ex.stack)
  process.exit(1)
})