'use strict';

// src/test-setup.js
import 'babel-polyfill';

import jsdom from 'jsdom';
const { JSDOM } = jsdom;

const { document } = (new JSDOM('')).window;
global.document = document;
global.window = document.defaultView;
global.navigator = window.navigator;