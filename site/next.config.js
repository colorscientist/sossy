'use strict';
const getRoutes = require('./routes');
const withImages = require('next-images');
const withOffline = require('next-offline');
const withCss = require('@zeit/next-css');

module.exports = withCss(withOffline(withImages({
  exportPathMap: getRoutes,
  webpack: (config) => {
    config.module.rules.push({
      test: /\.(png|svg|eot|otf|ttf|woff|woff2)$/,
      use: {
        loader: 'url-loader',
        options: {
          limit: 100000,
          publicPath: './',
          outputPath: 'static/',
          name: '[name].[ext]'
        }
      }
    })

    return config
  }
})));