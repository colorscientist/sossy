'use strict';
const routes = module.exports = require('next-routes')()

routes
.add('about')
.add({name: 'post', pattern: '/p/:url', page: 'post'})