require('./db').init(() => {
	const schema = require('./schema');
	const { json } = require('body-parser');
	const { graphqlExpress } = require('apollo-server-express');
	const { graphiqlExpress } = require('apollo-server-express');
	const express = require('express');
	const jwt = require('express-jwt');
	const cors = require('cors');
	const costAnalysis = require('graphql-cost-analysis').default;
	const polka = require('polka');
	const { db } = require('./db');

	const dev = process.env.NODE_ENV !== 'production';

	dev ? 
	console.log('Process started in dev mode') : 
	console.log('Process started in production mode');

	// Initialize the app
	const app = polka();
	class Context {
		constructor() {
			this.articleTotal = 0;

			db.collection(
				'posts').count(
					{public: { $eq: true }}).then(
						x => this.articleTotal = x);

			setInterval(
				() => {
					db.collection(
						'posts').count(
							{public: { $eq: true }}).then(
								x => this.articleTotal = x);
				},
				10000);
		}
	}

	const ctx = new Context();

	// The GraphQL endpoint
	app.use('/graphql', cors(), json(), graphqlExpress(req => { 
		return {
			schema,
			rootValue: null,
			context: ctx,
			validationRules: [
				costAnalysis({
					variables: req.body.variables,
					maximumCost:1000,
				}),
			]
		}}));

	if (dev) {
		// GraphiQL, a visual editor for queries
		app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }));
	}

	app.listen(4050, () => {
		console.log('Go to http://localhost:4050/graphiql to run queries!');
	}
)});