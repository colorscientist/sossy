const PostTypesEnum = Object.freeze({ARTICLE: 'Article', QUIZ: 'Quiz'})

// Takes a headline and returns the first 8 words, up to 120 characters,
// lowercase, no symbols, joined by dashes to create a friendly
// and readable URL
function makeURL(headline) {
    return headline.split(' ').slice(0, 8).join('-').toLowerCase().
    replace(/[`~!@#$%^&*()_|+\=?;:'",.<>\{\}\[\]\\\/]/gi, '').slice(0, 120)
}

module.exports = { makeURL, PostTypesEnum }