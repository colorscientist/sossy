const { MongoClient } = require('mongodb');

// Database object and settings
var db;
const ip = 'localhost';
const port = 27017;
const dbName = 'sossy';


module.exports.init = (callback) => {
  MongoClient.connect("mongodb://"+ip+":"+port, function (error, client) {
    if (!error) {
      console.log('Database online')
      db = client.db(dbName);
      module.exports.db = db;
    }
    callback();
  });
};