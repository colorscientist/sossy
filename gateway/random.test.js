import test from 'ava';
import { randomInt } from './build/Release/random';

test('getRandomInt returns random int', t => {
	const min = 0;
	const max = 10;
	t.is((10 >= randomInt(min, max) > 0), true);
});