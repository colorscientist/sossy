import test from 'ava';
import { makeURL } from './util';

test('makeURL takes sentence, turns it into URL', t => {
    const url = makeURL("Hello, world");
    t.is(url, 'hello-world');
});

test('makeURL keeps at most 120 characters', t => {
    const arr = new Array(200);
    const long = String(arr.fill('a'));
    const url = makeURL(long);
    t.is(url.length, 120);
});

test('makeURL keeps at most 8 words', t => {
    const url = makeURL('One two three four five six seven eight nine ten');
    t.is(url, 'one-two-three-four-five-six-seven-eight');
});

test('makeURL removes punctuation', t => {
    const url = makeURL("Hey you! Reading this URL? Click now, don't delay~");
    t.is(url, 'hey-you-reading-this-url-click-now-dont')
});

