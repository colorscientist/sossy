const { makeExecutableSchema } = require('graphql-tools');
const { importSchema } = require('graphql-import');
const resolvers = require('./resolvers');

const typeDefs = importSchema('schema/schema.graphql')

// Put together a schema
const schema = makeExecutableSchema({
    typeDefs,
    resolvers,
  });

module.exports = schema;