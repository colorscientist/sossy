const { makeURL } = require('./util');
const { PostTypesEnum } = require('./util');
const { randomInt } = require('./build/Release/random');
const { db } = require('./db');
const uuid = require('uuid/v4');

const resolvers = {
    Query: {
        posts: (parent, args, context, info) => {
            // For clarity to anyone who isn't familiar with js, 
            // the || (or) operator acts as a short circuit in this context,
            // using either the provided value, or falling back to a default
            return db.collection('posts').
            find({ postType: { $eq: args.input.postType }, public: { $eq: true }}).sort({_id:-1}).
            skip(args.input.skip || 0).limit(args.input.first || 10).toArray()
        },
        _postsMeta: (parent, args, context, info) => {
            const _postsMeta = {};
            (typeof args.input.postType !== 'undefined') ?
            (_postsMeta.count = db.collection('posts').count(
                { postType: { $eq: args.input.postType }, public: { $eq: true }})) :
            (_postsMeta.count = db.collection('posts').count(
                {public: { $eq: true }}))
            
            return _postsMeta
        },
        post: (parent, args, context, info) => {
            return db.collection('posts').
            findOne({ url: { $eq: args.input.url }, public: { $eq: true }})
        },
        random: (parent, args, context, info) => {
            return db.collection('posts').
            find({ public: { $eq: true }}).skip(randomInt(0, context.articleTotal)).limit(1).toArray();
        }
    },
    Mutation: {
        addArticle: (parent, args, context, info) => {
            var article = {};
            article.id = uuid();
            article.title = args.input.title;
            article.body = args.input.body;
            article.postType = PostTypesEnum.ARTICLE;
            article.createdAt = Date.now();
            article.public = false;
            article.url = makeURL(
                args.input.title);
            db.collection(
                'posts').insertOne(article);
            return article
        },
        editArticle: (parent, args, context, info) => {
            db.collection('posts').findOneAndUpdate(
                { id: { $eq: args.id }}, 
                {
                    $set: {
                      title: args.input.title,
                      body: args.input.body,
                      url: makeURL(
                          args.input.title),
                      lastEdited: Date.now()
                    }
                  });
        },
        addQuiz: (parent, args, context, info) => {
            var quiz = {}
            quiz.id = uuid();
            quiz.title = args.input.title;
            quiz.url = makeURL(args.input.title);
            quiz.postType = PostTypesEnum.QUIZ;
            quiz.createdAt = Date.now();
            quiz.public = false;
            quiz.questions = args.input.questions.map(
                q => {
                    q.id = uuid();
                    q.options = q.options.map(o => {
                        o.id = uuid();
                        return o
                    })
                    return q
                }
            )
            quiz.results = args.input.results.map(
                r => {
                    r.id = uuid();
                    return r
                }
            )
            db.collection('posts').insertOne(quiz);
            return quiz
        },
        makePublic: (parent, args, context, info) => {
            db.collection('posts').updateOne(
                { $or: [ {id: { $eq: args.input.id }}, {url: {$eq: args.input.url}}] }, {$set: {public: true}}).then(x => {return x})
        },
        makePrivate: (parent, args, context, info) => {
            return db.collection('posts').updateOne(
                { $or: [ {id: { $eq: args.input.id }}, {url: {$eq: args.input.url}}] }, {$set: {public: false}});
        }
    },
    Post: {
        // Allows the API to differentiate between types of Posts
        __resolveType: (post) => post.postType ? post.postType : null
    }
};

module.exports = resolvers;