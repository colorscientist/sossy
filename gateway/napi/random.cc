#include <node_api.h>
#include <random>
#include <assert.h>

namespace sossyrand {
    napi_value RandomInt(napi_env env, napi_callback_info info) {
        napi_status status;

        size_t argc = 2;
        napi_value args[2];
        status = napi_get_cb_info(env, info, &argc, args, nullptr, nullptr);
        assert(status == napi_ok);

        if (argc < 2) {
            napi_throw_type_error(env, nullptr, "Wrong number of arguments");
            return nullptr;
        }

        napi_valuetype valuetype0;
        status = napi_typeof(env, args[0], &valuetype0);
        assert(status == napi_ok);

        napi_valuetype valuetype1;
        status = napi_typeof(env, args[1], &valuetype1);
        assert(status == napi_ok);

        if (valuetype0 != napi_number || valuetype1 != napi_number) {
            napi_throw_type_error(env, nullptr, "Wrong arguments");
            return nullptr;
        }

        int min;
        status = napi_get_value_int32(env, args[0], &min);
        assert(status == napi_ok);

        int max;
        status = napi_get_value_int32(env, args[1], &max);
        assert(status == napi_ok);

        std::random_device rd;  //Will be used to obtain a seed for the random number engine
        std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
        std::uniform_int_distribution<> dis(min, max-1);

        int intgen;
        napi_value out;
        intgen = dis(gen);
        status = napi_create_int64(env, intgen, &out);
        assert(status == napi_ok);

        return out;
    }

    #define DECLARE_NAPI_METHOD(name, func)                          \
    { name, 0, func, 0, 0, 0, napi_default, 0 }

    napi_value Init(napi_env env, napi_value exports) {
        napi_status status;
        napi_property_descriptor randomIntDescriptor = DECLARE_NAPI_METHOD("randomInt", RandomInt);
        status = napi_define_properties(env, exports, 1, &randomIntDescriptor);
        assert(status == napi_ok);
        return exports;
    }
    
    NAPI_MODULE(NODE_GYP_MODULE_NAME, Init)
}